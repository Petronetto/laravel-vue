<!DOCTYPE html>
<html>
    <head>
        <title>Laravel</title>
        <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">
    </head>
    <body>
        <div class="container">
            <tasks></tasks>
        </div>

        <template id="tasks-template">
            <h1>My Tasks</h1>
                <li class="list-group-item" v-for="task in list">
                    @{{ task.body }}
                </li>
        </template>

        <script src="http://code.jquery.com/jquery.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/vue/1.0.8/vue.js"></script>
        <script src="/js/main.js"></script>
    </body>
</html>
