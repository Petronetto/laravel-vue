<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Task;

class TaskController extends Controller
{
    public function index()
    {
        //$task = Task::latest()->get();
        //return view('welcome')->with('tasks', $task);
        return Task::latest()->get();
    }
}
